package org.jpanku;

import org.apache.kafka.clients.admin.AdminClient;
import org.apache.kafka.clients.admin.DescribeClusterResult;
import org.apache.kafka.clients.consumer.*;
import org.apache.kafka.common.KafkaFuture;
import org.apache.kafka.common.Node;
import org.apache.kafka.common.PartitionInfo;
import org.apache.kafka.common.TopicPartition;
import org.apache.kafka.common.config.ConfigException;
import org.apache.kafka.common.errors.WakeupException;
import org.junit.jupiter.api.*;

import java.io.FileInputStream;
import java.io.IOException;
import java.time.Duration;
import java.util.*;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.IntStream;

import static org.apache.kafka.clients.admin.AdminClient.create;
import static org.junit.jupiter.api.Assertions.*;

/**
 * Main testing class
 */
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class MainTest {

    /** Properties file's path + name */
    private static final String PROPERTIES_FILE = "config/java.config";

    /** Properties file handler */
    private static final Properties properties = new Properties();

    /** Time in seconds the tests should wait to succeed, and if not, it should fail by timeout. */
    private static int testBrokerTimeout;

    /** Topic for the consumer to subscribe. */
    private static String topic;

    /** Group id for the configured topic. */
    private static String groupId;

    /** Tells other test cases to not execute when there is not a valid broker running. */
    private static Boolean validBrokerExist = false;

    /** Tells other test cases to not execute when the topic is not a valid topic. */
    private static Boolean validTopicExist = false;

    /** Time in seconds for the consumer to poll the topic */
    private static int pollingTime;

    /**
     * Main method to consume records from the broker.
     *
     * @param consumer Broker consumer.
     */
    private void consume(
            final Consumer<String, String> consumer,
            final int pollingTime
    ) {
        // We use atomic boolean because we are going to share the value between threads.
        final AtomicBoolean closed = new AtomicBoolean(false);

        // We start consumer stopper thread.
        println("Polling during " + pollingTime + " seconds ...");
        startConsumerStopper(consumer, closed, pollingTime);

        // We start polling
        final int totalRecords = startPolling(consumer, closed);
        if (totalRecords == 0) println(">>>> No records found during polling.");
        else println(">>>> Total records consumed: " + totalRecords);

        consumer.close();
    }

    /**
     * @param key Key to look for
     * @return The value for the corresponding key, empty string if not present
     */
    private static String getFromProperties(final String key, final String defaultValue) {
        return properties.getProperty(key, defaultValue);
    }

    /** Prints param str to stdout */
    private static void println(final String str) {
        System.out.println(str);
    }

    /**
     * Creates a new record with supplied parameters.
     *
     * @return New record
     */
    private ConsumerRecord<String, String> record(
            final String topic,
            final int partition,
            final int offset,
            final String key,
            final String value
    ) {
        return new ConsumerRecord<>(topic, partition, offset, key, value);
    }

    /** Schedule a task to add records to the consumer associated to a partition. */
    private void scheduleAddRecords(
            final int partition,
            final MockConsumer<String, String> consumer
    ) {
        consumer.schedulePollTask(() -> IntStream.range(0, 10).forEach(n -> consumer.addRecord(
                record(topic,
                        partition,
                        n,
                        "<<MockRecordKey " + n + ">>",
                        "<<MockRecordValue " + n + ">>"
                )
        )));
    }

    /**
     * Start a delay equal to timeout variable in seconds. When time is done, it sends signal to
     * Kafka consumer's poll loop to stop by setting closed true, and finally it sends wakeup
     * signal to consumer to stop current poll.
     *
     * @param consumer Kafka consumer
     * @param closed   Flag intended to stop poll loop
     */
    private void startConsumerStopper(
            final Consumer<String, String> consumer,
            final AtomicBoolean closed,
            final int pollingTime
    ) {
        final Thread consumerCloser = new Thread(() -> {
            try {
                TimeUnit.SECONDS.sleep(pollingTime);
                closed.set(true);
                consumer.wakeup();
            } catch (InterruptedException e) {
                fail(e.getMessage(), e);
            }
        });
        consumerCloser.start();
    }

    /**
     * Start the kafka consumer polling and show in stdout some date about records consumed.
     *
     * @param consumer Kafka consumer
     * @param closed   Flag intended to stop poll loop
     * @return Total number of records consumed
     */
    private int startPolling(
            final Consumer<String, String> consumer,
            final AtomicBoolean closed
    ) {
        int totalRecords = 0;
        try {
            while (!closed.get()) {
                final ConsumerRecords<String, String> records =
                        consumer.poll(Duration.ofMillis(100));

                long currentCount = 0L;
                for (ConsumerRecord<String, String> record : records) {
                    totalRecords++;
                    println("Consumed record with key " + record.key() +
                            " and value " + record.value() +
                            ", and updated total count to " + currentCount++);
                }
            }
        } catch (WakeupException e) {
            // Ignore exception if closing
            if (!closed.get()) throw e;
        }
        return totalRecords;
    }

    @BeforeAll
    static void setUp() {
        println("Executing setup...");
        try {
            properties.load(new FileInputStream(PROPERTIES_FILE));

            final String bootstrapHost =
                    getFromProperties("bootstrap.servers.host", "localhost");
            final String bootstrapPort =
                    getFromProperties("bootstrap.servers.port", "9092");

            properties.setProperty("bootstrap.servers", bootstrapHost + ":" + bootstrapPort);
            properties.setProperty("client.id", "producerAdmin");
            properties.setProperty("metadata.max.age.ms", "3000");

            testBrokerTimeout =
                    Integer.parseInt(
                            getFromProperties("testbroker.timeout", "10"));

            topic = getFromProperties("topic", "");
            groupId = getFromProperties("group.id", "test-consumer-group");

            pollingTime =
                    Integer.parseInt(getFromProperties("polling.time", "10"));
        } catch (IOException | NumberFormatException e) {
            fail(e.getMessage(), e);
        }
    }

    /**
     * Check that there is at least one valid broker running in kafka server.
     * It is intended as a way to check kafka server up and running.
     */
    @Test
    @DisplayName("There is a valid broker running.")
    @Order(1)
    void validBrokerExist() {
        println("Validating broker...");

        try (final AdminClient kafkaAdminClient = create(properties)) {
            DescribeClusterResult clusterDescription = kafkaAdminClient.describeCluster();
            KafkaFuture<Collection<Node>> kafkaFuture = clusterDescription.nodes();

            // Every node represents a valid broker in kafka server.
            Collection<Node> nodeList = kafkaFuture.get(testBrokerTimeout, TimeUnit.SECONDS);

            assertNotNull(nodeList, "Brokers list is null");
            assertFalse(nodeList.isEmpty(), "Brokers list is empty");
        } catch (ExecutionException | InterruptedException | TimeoutException e) {
            fail(e.getMessage(), e);
        }
        validBrokerExist = true;
    }

    /** Check if the configured topic is a valid topic for the kafka consumer to subscribe. */
    @Test
    @DisplayName("Is the configured topic valid")
    @Order(2)
    void validTopic() {
        println("Validating topic...");

        // This test executes only if the broker configured exists and is valid
        Assumptions.assumeTrue(
                validBrokerExist,
                "Test not executed because there is not a valid broker running");

        properties.setProperty(
                ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG,
                "org.apache.kafka.common.serialization.StringDeserializer");

        properties.setProperty(
                ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG,
                "org.apache.kafka.common.serialization.StringDeserializer");

        try (Consumer<String, String> consumer = new KafkaConsumer<>(properties)) {
            Map<String, List<PartitionInfo>> topicList = consumer.listTopics();

            /* If there is a valid broker running at least __consumer_offsets topic will be there.
             * (kafka version 5.8.2) */
            assertTrue(
                    topicList.containsKey(topic),
                    "There is no topic named " + topic + " created on the server");

            validTopicExist = true;
        } catch (ConfigException e) {
            fail(e.getMessage(), e);
        }
    }

    /**
     * Subscribe the kafka mock consumer, start polling and show some
     * information about the consumed records. All of this using a mock consumer.
     */
    @Test
    @DisplayName("[Using Mock] Can subscribe and poll the configured topic")
    @Order(3)
    void consumeBrokerMock() {
        println("Starting mock test...");

        int partition = 0;
        final MockConsumer<String, String> consumer =
                new MockConsumer<>(OffsetResetStrategy.EARLIEST);

        // We cannot add records before assigning a partition or subscribing to a topic.
        // In this case we will assign partition.
        consumer.assign(Collections.singleton(new TopicPartition(topic, partition)));

        // We set up the start of the offsets
        HashMap<TopicPartition, Long> startOffsets = new HashMap<>();
        TopicPartition tp = new TopicPartition(topic, partition);
        startOffsets.put(tp, 0L);
        consumer.updateBeginningOffsets(startOffsets);

        // We schedule a task to add the test records to the broker.
        scheduleAddRecords(partition, consumer);

        // Consume broker
        consume(consumer, 5);
    }

    /**
     * Subscribe the kafka consumer to the configured topic, start polling and show some
     * information about the consumed records.
     */
    @Test
    @DisplayName("Can subscribe and poll the configured topic for one minute")
    @Order(4)
    void consumeBroker() {
        println("Starting main broker consuming test...");

        // This test executes only if the broker configured exists and is valid
        Assumptions.assumeTrue(
                validTopicExist,
                "Test not executed because there is not a valid broker running");

        properties.setProperty(
                ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG,
                "org.apache.kafka.common.serialization.StringDeserializer");

        properties.setProperty(
                ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG,
                "org.apache.kafka.common.serialization.StringDeserializer");

        properties.setProperty(ConsumerConfig.GROUP_ID_CONFIG, groupId);

        final Consumer<String, String> consumer = new KafkaConsumer<>(properties);

        // We cannot add records before assigning a partition or subscribing to a topic.
        // In this case we will subscribe to the configured topic.
        consumer.subscribe(List.of(topic));

        // Consume broker
        consume(consumer, pollingTime);
    }

    @AfterAll
    static void tearDown() {

    }
}