package org.jpanku;

import org.apache.kafka.clients.admin.AdminClient;
import org.apache.kafka.clients.admin.NewTopic;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.errors.TopicExistsException;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Collections;
import java.util.Optional;
import java.util.Properties;
import java.util.concurrent.ExecutionException;

public class Main {

    /** Properties file's path + name */
    private static final String PROPERTIES_FILE = "config/java.config";

    /** Properties file handler */
    private static final Properties properties = new Properties();

    /** Topic for the consumer to subscribe. */
    private static String topic;

    /** Exception when trying to create a topic in the broker. */
    static class CreateTopicException extends Exception {
        public CreateTopicException(String message) {
            super(message);
        }
    }

    /**
     * @param key Key to look for
     * @return The value for the corresponding key, empty string if not present
     */
    private static String getFromProperties(
            String key,
            String defaultValue
    ) {
        return properties.getProperty(key, defaultValue);
    }

    /** Prints param str to stdout */
    private static void println(String str) {
        System.out.println(str);
    }

    /** Load properties from configured file and topic variable. */
    private static void setup() throws IOException {
        try (InputStream inputStream = Files.newInputStream(Path.of(PROPERTIES_FILE))) {
            properties.load(inputStream);
            String bootstrapHost = getFromProperties("bootstrap.servers.host", "localhost");
            String bootstrapPort = getFromProperties("bootstrap.servers.port", "9092");
            properties.setProperty("bootstrap.servers", bootstrapHost + ":" + bootstrapPort);
            properties.setProperty("client.id", "producerAdmin");
            properties.setProperty("metadata.max.age.ms", "3000");
            topic = getFromProperties("topic", "");
        }
    }

    /**
     * Create topic in the broker, ignores if topic exist.
     *
     * @param topic Configured topic
     */
    public static void createTopic(final String topic)
            throws CreateTopicException {
        final NewTopic newTopic =
                new NewTopic(topic, Optional.empty(), Optional.empty());
        try (final AdminClient adminClient = AdminClient.create(properties)) {
            adminClient.createTopics(Collections.singletonList(newTopic)).all().get();
        } catch (final InterruptedException | ExecutionException e) {
            // Ignore if TopicExistsException, which may be valid if topic exists
            if (!(e.getCause() instanceof TopicExistsException)) {
                Thread.currentThread().interrupt();
                throw new CreateTopicException(e.getMessage());
            }
        }
    }

    /** main method */
    public static void main(String[] args) {
        println("Main.main method executing...");

        try {
            // Loading properties
            setup();

            // Create topic
            createTopic(topic);

        } catch (IOException | CreateTopicException e) {
            println(e.getMessage());
            return;
        }

        // Add additional properties.
        properties.put(ProducerConfig.ACKS_CONFIG, "all");
        properties.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringSerializer");
        properties.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringSerializer");

        // Produce test data
        try (Producer<String, String> producer = new KafkaProducer<>(properties)) {
            // We create 10 records to produce
            for (int i = 0; i < 10; i++) {
                final String key = "<<TestKey" + i + ">>";
                final String value = "<<TestValue " + i + ">>";
                println("Producing record: " + key + " - " + value);
                producer.send(new ProducerRecord<>(topic, key, value), (metadata, exception) -> {
                    if (exception != null) exception.printStackTrace();
                    else
                        println("Produced record to" +
                                " topic " + metadata.topic() +
                                " partition " + metadata.partition() +
                                " and offset " + metadata.offset());

                });
            }
        }
    }
}