# Java Broker Tester

Java tests to check connectivity to a valid kafka broker. 
Includes Producer functionality to create records which can be read by Consumer tests.

## Requirements
* Java 11
* Maven (Tested with maven version 3.8.5)
* A Kafka Server running a valid Broker with constant message traffic. (Tested with Kafka 
version 2.13-3.2.0)

## Usage Guide

Download the project

`git clone git@gitlab.com:jpanku/javabrokertester.git`

From the same directory where you downloaded change into the project directory: 

`cd javabrokertester`

Clean and compile

`mvn clean compile`

Open `config/java.config` file and edit according to the setup of the Kafka server you will use.
Recommended is you set `polling.time` to at least 60 seconds to check activity. Topic is needed to
be changed according to topic configured in the broker.

### Using tests directly

Run tests

`mvn test`

You should se something like this fragment in the screen if the broker don't have traffic, and you didn't 
change default values. If the broker have records produced in the configured topic you should also 
see those records consumed.

```
[INFO] Running org.jpanku.MainTest
Executing setup...
Validating broker...
Validating topic...
Starting mock test...
Polling during 5 seconds ...
Consumed record with key <<MockRecordKey 0>> and value <<MockRecordValue 0>>, and updated total count to 0
Consumed record with key <<MockRecordKey 1>> and value <<MockRecordValue 1>>, and updated total count to 1
Consumed record with key <<MockRecordKey 2>> and value <<MockRecordValue 2>>, and updated total count to 2
Consumed record with key <<MockRecordKey 3>> and value <<MockRecordValue 3>>, and updated total count to 3
Consumed record with key <<MockRecordKey 4>> and value <<MockRecordValue 4>>, and updated total count to 4
Consumed record with key <<MockRecordKey 5>> and value <<MockRecordValue 5>>, and updated total count to 5
Consumed record with key <<MockRecordKey 6>> and value <<MockRecordValue 6>>, and updated total count to 6
Consumed record with key <<MockRecordKey 7>> and value <<MockRecordValue 7>>, and updated total count to 7
Consumed record with key <<MockRecordKey 8>> and value <<MockRecordValue 8>>, and updated total count to 8
Consumed record with key <<MockRecordKey 9>> and value <<MockRecordValue 9>>, and updated total count to 9
>>>> Total records consumed: 10
Starting main broker consuming test...
Polling during 10 seconds ...
>>>> No records found during polling.
[INFO] Tests run: 4, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 16.473 s - in org.jpanku.MainTest
```

Explanation (Steps/Tests):
* Setup, which load configuration values from configuration file.
* Validate if there is a valid broker.
* Validate if configured topic is created in broker.
* Consume some records using a Broker Mock.
* Polling and consuming records from the topic and broker configured.

# Running Producer (Optional)

In case you have a valid broker, but you don't have traffic (For example if you just installed a 
local Kafka server), you can use a producer functionality included in this project.

In the same project directory you execute:

`mvn compile exec:java -Dexec.mainClass="org.jpanku.Main"`

This command will create 10 records to the same configured topic, and if the topic does not exist,
it will create it.

You can run this producer all the times you want, and it can be run before you run the tests, or at 
the same time you are running polling from tests in another terminal, records should be consumed
right away, and should be showed in screen.